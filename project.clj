(defproject clj-bmi "0.1.0-SNAPSHOT"
  :description "BMI Calculator"
  :url "https://bitbucket.org/vajracoffee/clj-bmi"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :main ^:skip-aot clj-bmi.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
