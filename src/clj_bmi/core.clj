(ns clj-bmi.core
  (:import (javafx.scene SceneBuilder)
           (javafx.scene.control ButtonBuilder)
           (javafx.scene.layout VBoxBuilder)
           (javafx.stage StageBuilder))
  (:gen-class))

;;;; Some examples and docs.
;;;; None of then is completely understandable, at least for me
;;;; Comming from Python and HTML/JS/CSS background, I'm lost most of the time.
;;;; Hope I can leave a good example of code that  others can learn.
; https://docs.oracle.com/javafx/2/api/
; http://coldnew.github.io/blog/2013/03-09_4add8/
; https://github.com/raju-bitter/clojure-javafx-example
; https://coderwall.com/p/4yjy1a/getting-started-with-javafx-in-clojure
; https://gist.github.com/zilti/6286307
; https://github.com/zilti/clojurefx
; https://github.com/daveray/upshot/blob/develop/src/upshot/core.clj
; http://coherentminds.com/blog/2013/11/24/your-first-javafx-app/
; http://www.falkoriemenschneider.de/a__2014-05-01__Applying-core-async-to-JavaFX.html
; https://github.com/friemen/async-ui/blob/master/src/async_ui/ex_master_detail.clj
; http://drowsy.de/blog/?p=7


; Porposed by: https://github.com/daveray/upshot/blob/develop/src/upshot/core.clj
; followed by others.
;*******************************************************************************
; A hack to force initialization of the JavaFX toolkit. Otherwise, you
; can't do anything outside of an Application sub-class which isn't fun
; for the REPL.
; TODO do something else.
(defonce force-toolkit-init (javafx.embed.swing.JFXPanel.))

(defn run-later*
  [f]
  (javafx.application.Platform/runLater f))

(defmacro run-later
  [& body]
  `(run-later* (fn [] ~@body)))

(defn run-now*
  [f]
  (let [result (promise)]
    (run-later
      (deliver result (try (f) (catch Throwable e e))))
    @result))

(defmacro run-now
  [& body]
  `(run-now* (fn [] ~@body)))

;*******************************************************************************

(defn event-handler*
  [f]
  (reify javafx.event.EventHandler
    (handle [this e] (f e))))

(defmacro event-handler [arg & body]
  `(event-handler* (fn ~arg ~@body)))


(def stage (atom nil))


(def hello-button (.. ButtonBuilder create
                (text "Say \"Hello Clojure\"!")
                (onAction (event-handler [_] (println "Hello Clojure!")))
                build))

(def quit-button (.. ButtonBuilder create
                     (text "Quit!")
                     (onAction (event-handler [_] (.close @stage)))
                     build))

;; You of course don't have to write it all in one block.
;; Using a (def button (.. ButtonBuilder ...)) and then adding button is just as good (probably better most of the times).
(run-now (reset! stage (.. StageBuilder create
                           (title "Hello JavaFX")
                           (scene (.. SceneBuilder create
                                      (height 480) (width 640)
                                      (root (.. VBoxBuilder create
                                                (minHeight 480) (minWidth 640)
                                                (children [hello-button quit-button])
                                                build))
                                      build))
                           build)))

;(run-now (.show @stage))

(defn -main
  [& args]
  (run-now (.showAndWait @stage)))
